﻿
/**
 * Provides a utility to generate random key of variable sizes
 *
 * @export
 * @class RandomKeyGenerator
 */
export default class RandomKeyGenerator {
    /**
     * Returns a ramdom key made from aA-zZ and 0-9
     * http://stackoverflow.com/a/1349426
     *
     * @static
     * @param {number} [size=20]
     * @returns {string}
     * @memberof RandomKeyGenerator
     */
    public static GenerateKey(size: number = 20): string {
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (let i = 0; i < size; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
}
