/**
 * Common utilities for objects
 *
 * @export
 * @class ObjectUtils
 */
export default class ObjectUtils {

    /**
     * Returns true if the object is null or undefined
     *
     * @static
     * @param {*} obj
     * @returns {boolean}
     * @memberof ObjectUtils
     */
    public static IsNullOrUndefined(obj: any): boolean {
        return obj === null || obj === undefined;
    }
}
