
/**
 * Array Util class for easy
 * access to common functionality
 *
 * @export
 * @class ArrayUtils
 */
export default class ArrayUtils {

    /**
     * Returns true if the array is 
     * null, undefined or of 0 size
     *
     * @static
     * @param {any[]} array
     * @returns
     * @memberof ArrayUtils
     */
    public static IsNullOrEmpty(array: any[]): boolean {
        return array === null ||
            array === undefined ||
            array.length === 0;
    }


    /**
     * Returns the size of the array
     * or 0 if its empty
     *
     * @static
     * @param {any[]} array
     * @returns {number}
     * @memberof ArrayUtils
     */
    public static GetLength(array: any[]): number {
        if (this.IsNullOrEmpty(array)) {
            return 0;
        }

        return array.length;
    }
}
