import NotificationService from "~/Core/Framework/NotificationService/NotificationService";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";


/**
 * Abstract data store class
 * to provide common functionality
 * to in memory storage
 *
 * @abstract
 * @class DataStore
 * @template T
 */
abstract class DataStore<T> {
    private notificationService: NotificationService;

    private data: T[] = [];

    public get Data(): T[] {
        return this.data;
    }

    /**
     * Data Store Item Added notification name
     *
     * @readonly
     * @protected
     * @abstract
     * @type {string}
     * @memberof DataStore
     */
    protected abstract get DataStoreItemAddedNotification(): string;


    /**
     * Data Store Item Removed notification name
     *
     * @readonly
     * @protected
     * @abstract
     * @type {string}
     * @memberof DataStore
     */
    protected abstract get DataStoreItemRemovedNotification(): string;

    /**
     * Data Store Items Updated Notification
     *
     * @readonly
     * @protected
     * @abstract
     * @type {string}
     * @memberof DataStore
     */
    protected abstract get DataStoreItemsUpdatedNotification():string;

    /**
     *Creates an instance of DataStore.
     * @param {NotificationService} notificationService
     * @memberof DataStore
     */
    constructor(notificationService: NotificationService) {
        this.notificationService = notificationService;
    }


    /**
     * Abstract method to get an item by
     * its id
     *
     * @abstract
     * @param {string} id
     * @returns {T}
     * @memberof DataStore
     */
    public abstract GetById(id: string): T;


    /**
     * Adds an item to a data store
     *
     * @param {T} item
     * @returns
     * @memberof DataStore
     */
    public Add(item: T) {
        if (
            ObjectUtils.IsNullOrUndefined(item) ||
            !ObjectUtils.IsNullOrUndefined(
                this.GetById(this.GetIdForItem(item))
            )
        ) {
            return;
        }

        this.Data.push(item);
        
        this.SendNotification(this.DataStoreItemAddedNotification, item);
        this.SendNotification(this.DataStoreItemsUpdatedNotification, item);
    }


    /**
     * Removed an item from the data store
     *
     * @param {T} item
     * @returns
     * @memberof DataStore
     */
    public Remove(item: T) {
        if (ObjectUtils.IsNullOrUndefined(item)) {
            return;
        }

        const id: string = this.GetIdForItem(item);

        for (let i = 0; i < this.Data.length; i++) {
            const storedItem: T = this.Data[i];
            if (this.GetIdForItem(storedItem) == id) {
                this.Data.splice(i, 1);
                this.SendNotification(
                    this.DataStoreItemRemovedNotification,
                    storedItem
                );
                break;
            }
        }

        this.SendNotification(this.DataStoreItemsUpdatedNotification, item);
    }


    /**
     * Gets the id for a given item
     *
     * @protected
     * @abstract
     * @param {T} item
     * @returns {string}
     * @memberof DataStore
     */
    protected abstract GetIdForItem(item: T): string;


    /**
     * Send a notification to the system
     *
     * @protected
     * @param {string} name
     * @param {*} [body]
     * @param {*} [senderKey]
     * @returns
     * @memberof DataStore
     */
    protected SendNotification(name: string, body?: any, senderKey?: any) {
        if (ObjectUtils.IsNullOrUndefined(name)) {
            return;
        }

        this.notificationService.SendNotification(name, body, senderKey);
    }
}

export default DataStore;
