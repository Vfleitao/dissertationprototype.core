
/**
 * Orientation enum used for
 * container and inheriting classes
 *
 * @enum {number}
 */
enum Orientation {
    Vertical = 0,
    Horizontal = 1
}

export default Orientation;