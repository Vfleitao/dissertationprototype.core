/**
 * Event Types use to delegate an event type 
 *
 * @enum {number}
 */
enum EventType { 
    Click,
    SwitchStateChange
}

export default EventType;