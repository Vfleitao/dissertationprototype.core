import INotificationHandler from "~/Core/Framework/NotificationService/NotificationHandler";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import Injectable from "~/Core/Framework/IOCContainer/Injectable";


/**
 * Notification Service used to propagate
 * notifications across the system
 * and registering handlers
 *
 * @export
 * @class NotificationService
 */
@Injectable("NotificationService")
export default class NotificationService {

    /**
     * Registration of notification handlers
     *
     * @private
     * @type {{ [notificationName: string]: INotificationHandler[] }}
     * @memberof NotificationService
     */
    private registry: { [notificationName: string]: INotificationHandler[] } = {};


    /**
     * Registers a new notification handler
     *
     * @param {INotificationHandler} handler
     * @returns
     * @memberof NotificationService
     */
    public async Register(handler: INotificationHandler) {
        if (ObjectUtils.IsNullOrUndefined(handler)) {
            throw new Error("handler cannot be null");
        }

        if (ObjectUtils.IsNullOrUndefined(this.registry[handler.NotificationName])) {
            this.registry[handler.NotificationName] = [];
        }

        if (await this.IsHandlerCreated(this.registry[handler.NotificationName], handler)) {
            return;
        }

        this.registry[handler.NotificationName].push(handler);
    }


    /**
     * Unregisters a new notification handler
     *
     * @param {INotificationHandler} handler
     * @returns
     * @memberof NotificationService
     */
    public UnRegister(handler: INotificationHandler) {
        if (ObjectUtils.IsNullOrUndefined(handler)) {
            throw new Error("handler cannot be null");
        }

        const handlerList: INotificationHandler[] = this.registry[handler.NotificationName];

        if (ObjectUtils.IsNullOrUndefined(handlerList)
            || !this.IsHandlerCreated(handlerList, handler)) {
            return;
        }

        for (let i: number = 0, ln: number = handlerList.length; i < ln; i++) {
            const currentHandler: INotificationHandler = handlerList[i];
            if (currentHandler.Context.ContextKey === handler.Context.ContextKey
                && currentHandler.NotificationName === handler.NotificationName) {
                handlerList.splice(i, 1);
                return;
            }
        }
    }


    /**
     * Send a notification to the whole system
     *
     * @param {string} notificationName
     * @param {*} [body]
     * @param {string} [senderKey]
     * @returns
     * @memberof NotificationService
     */
    public SendNotification(notificationName: string, body?: any, senderKey?: string) {
        const handlerList: INotificationHandler[] = this.registry[notificationName];

        if (ObjectUtils.IsNullOrUndefined(handlerList)) {
            return;
        }

        const handlers: Array<(name: string, body?: any, senderKey?: any) => void> = handlerList.map((handler) => handler.NotificationHandler);
        handlers.forEach((handler) => {
            handler(notificationName, body, senderKey);
        });

    }


    /**
     * returns if a notification is registered with a handler
     * or not
     *
     * @private
     * @param {INotificationHandler[]} handlerList
     * @param {INotificationHandler} handler
     * @returns {boolean}
     * @memberof NotificationService
     */
    private IsHandlerCreated(handlerList: INotificationHandler[], handler: INotificationHandler): boolean {
        for (let i: number = 0, ln: number = handlerList.length; i < ln; i++) {
            const currentHandler: INotificationHandler = handlerList[i];
            if (currentHandler.Context === handler.Context
                && currentHandler.NotificationName === handler.NotificationName) {
                return true;
            }
        }
        return false;
    }

}
