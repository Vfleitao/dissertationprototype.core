import INotificationHandlerContext from "~/Core/Framework/NotificationService/INotificationHandlerContext";


/**
 * Notification handler interface
 *
 * @export
 * @interface INotificationHandler
 */
export default interface  INotificationHandler {
    NotificationName: string;
    NotificationHandler: (name: string, body?: any, senderKey?: any) => void;
    Context: INotificationHandlerContext;
}
