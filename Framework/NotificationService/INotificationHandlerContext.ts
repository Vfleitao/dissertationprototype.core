/**
 * Notification context used
 * to identify individual notification handlers
 *
 * @export
 * @interface INotificationHandlerContext
 */
export default interface INotificationHandlerContext {
    ContextKey: string;
}
