import IEventDelegator from "~/Core/Framework/Events/IEventDelegator";
import EventType from "~/Core/Framework/Enums/EventType";
import Component from "~/Core/Framework/Components/Component";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import Injectable from "~/Core/Framework/IOCContainer/Injectable";


/**
 * Event processor class which delegates to IEventDelegator
 * the adding and removing of events
 *
 * @export
 * @class EventProcessor
 */
@Injectable("EventProcessor")
export default class EventProcessor {
    private delegators: { [id: number]: IEventDelegator } = {};


    /**
     * Registers a new delegator
     *
     * @param {EventType} type
     * @param {IEventDelegator} delegator
     * @memberof EventProcessor
     */
    public AddNewEventDelegator(type: EventType, delegator: IEventDelegator) {
        this.delegators[type] = delegator;
    }


    /**
     * Registers an event to a given IEventDelegator
     *
     * @param {EventType} type
     * @param {Component} component
     * @param {((value?: any) => void | Function)} handler
     * @returns
     * @memberof EventProcessor
     */
    public RegisterEvent(type: EventType, component: Component, handler: (value?: any) => void | Function) {
        if (ObjectUtils.IsNullOrUndefined(this.delegators[type])) {
            return;
        }

        const eventDelegator: IEventDelegator = this.delegators[type];
        
        try {
            eventDelegator.RegisterEvent(component, handler);
        } catch {
        }
    }


    /**
     * Unregisters an event
     *
     * @param {EventType} type
     * @param {Component} component
     * @param {((value?: any) => void | Function)} handler
     * @returns
     * @memberof EventProcessor
     */
    public UnRegisterEvent(type: EventType, component: Component, handler: (value?: any) => void | Function) {
        if (ObjectUtils.IsNullOrUndefined(this.delegators[type])) {
            return;
        }

        try {
            const eventDelegator: IEventDelegator = this.delegators[type];
            eventDelegator.UnRegisterEvent(component, handler);
        } catch {
        }
    }
}