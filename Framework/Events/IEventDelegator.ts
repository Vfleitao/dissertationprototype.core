import Component from "~/Core/Framework/Components/Component";


/**
 * Event Delegator interface
 *
 * @interface IEventDelegator
 */
interface IEventDelegator {
    RegisterEvent(component: Component, handler: (value?: any) => void | Function);
    UnRegisterEvent(component: Component, handler: (value?: any) => void | Function);
}

export default IEventDelegator;