import NotificationService from "~/Core/Framework/NotificationService/NotificationService";
import RandomKeyGenerator from "~/Core/Framework/Utils/RandomKeyGenerator";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";


/**
 * Generic service class which needs to be extended
 * by any services which need to run at any time during
 * the application execution
 *
 * @export
 * @class Service
 */
export default class Service {

    /**
     * notification service used to send notifications
     *
     * @private
     * @type {NotificationService}
     * @memberof Service
     */
    private notificationService: NotificationService;


    /**
     * Service ID
     *
     * @private
     * @type {string}
     * @memberof Service
     */
    private id: string;


    /**
     * Service ID getter
     *
     * @readonly
     * @type {string}
     * @memberof Service
     */
    public get Id(): string {
        return this.id;
    }


    /**
     *Creates an instance of Service.
     * @param {NotificationService} notificationService
     * @memberof Service
     */
    constructor(notificationService: NotificationService) {
        this.notificationService = notificationService;

        this.id = RandomKeyGenerator.GenerateKey();
        this.AddNotificationHandlers();
    }


    /**
     * Notification Handler adding method.
     * Can be overwritten by subclasses
     * to register notification handlers
     *
     * @protected
     * @memberof Service
     */
    protected AddNotificationHandlers() {
        
    }


    /**
     * Send a notification through the system
     *
     * @protected
     * @param {string} name
     * @param {*} [body]
     * @param {*} [senderKey]
     * @returns
     * @memberof Service
     */
    protected SendNotification(name: string, body?: any, senderKey?: any) {
        if (ObjectUtils.IsNullOrUndefined(name)) {
            return;
        }

        this.notificationService.SendNotification(name, body, senderKey);
    }


    /**
     * Registers a new notification handler for a given
     * notification name
     *
     * @protected
     * @param {string} notificationName
     * @param {(name: string, body?: any, senderKey?: any) =>void} handler
     * @returns
     * @memberof Service
     */
    protected RegisterNotification(notificationName: string, handler: (name: string, body?: any, senderKey?: any) =>void) {
        if (ObjectUtils.IsNullOrUndefined(notificationName) ||
            ObjectUtils.IsNullOrUndefined(handler)) {
            return;
        }

        this.notificationService.Register({
            NotificationName: notificationName,
            NotificationHandler: handler,
            Context: {
                ContextKey: this.Id
            }
        });
    }


    /**
     * Unregisters a given notification
     *
     * @protected
     * @param {string} notificationName
     * @param {(name: string, body?: any, senderKey?: any) => void} handler
     * @returns
     * @memberof Service
     */
    protected UnregisterNotification(notificationName: string, handler: (name: string, body?: any, senderKey?: any) => void) {
        if (ObjectUtils.IsNullOrUndefined(notificationName) ||
            ObjectUtils.IsNullOrUndefined(handler)) {
            return;
        }

        this.notificationService.UnRegister({
            NotificationName: notificationName,
            NotificationHandler: handler,
            Context: {
                ContextKey: this.Id
            }
        });
    }

}