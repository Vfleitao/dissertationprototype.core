import ModuleResolver from "~/Core/Framework/IOCCOntainer/ModuleResolver";

/**
 * Sets a given property with an instanciated object
 * registed in the IOC container with a given key
 *
 * @export
 * @param {string} injectionKey
 * @returns
 */
export default function Inject(injectionKey: string) {
  return function (target: any, decoratedPropertyName: string): void {
    target[decoratedPropertyName] = ModuleResolver.Instance.GetModuleSync(
      injectionKey
    );
  };
}
