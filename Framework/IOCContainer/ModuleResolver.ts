﻿/**
 * Module resolver used as an IOC container
 * Enabled registering and unregistering classes and
 * its given dependencies
 *
 * @export
 * @class ModuleResolver
 */
export default class ModuleResolver {
  public static Instance: ModuleResolver = new ModuleResolver();
  private library = [];


  /**
   * Registers a module while
   * getting dependencies synchronously
   *
   * @param {string} name
   * @param {any[]} dependencies
   * @param {*} module
   * @returns
   * @memberof ModuleResolver
   */
  public RegisterModuleSync(name: string, dependencies: any[], module: any) {
    if (!module) {
      return;
    }

    if (dependencies && dependencies.length) {
      let dependenciesForFunction: any[] = this.GetDependenciesSync(dependencies);
      this.library[name] = new module(...dependenciesForFunction);
    } else {
      this.library[name] = new module();
    }

  }


  /**
   * Registers a module and gets the dependencies asynchronously
   *
   * @param {string} name
   * @param {any[]} dependencies
   * @param {*} module
   * @returns
   * @memberof ModuleResolver
   */
  public async RegisterModule(name: string, dependencies: any[], module: any) {
    if (!module) {
      return;
    }
    if (dependencies && dependencies.length) {
      const dependenciesForFunction: any[] = await this.GetDependencies(
        dependencies
      );
      this.library[name] = new module(...dependenciesForFunction);
    } else {
      this.library[name] = new module();
    }
  }


  /**
   * Gets a module synchronously
   *
   * @param {string} moduleName
   * @returns {*}
   * @memberof ModuleResolver
   */
  public GetModuleSync(moduleName: string): any {
    let requestedModule: any = null;

    if (this.library[moduleName]) {
      requestedModule = this.library[moduleName];
    }

    return requestedModule;
  }


  /**
   * Gets the a module synchronously
   *
   * @param {string} moduleName
   * @returns {Promise<any>}
   * @memberof ModuleResolver
   */
  public async GetModule(moduleName: string): Promise<any> {
    return new Promise((resolve) => {
      let requestedModule: any = null;

      if (this.library[moduleName]) {
        requestedModule = this.library[moduleName];
      }

      resolve(requestedModule);
    });
  }


  /**
   * Gets depedencies synchronously
   *
   * @private
   * @param {any[]} dependencies
   * @returns {any[]}
   * @memberof ModuleResolver
   */
  private GetDependenciesSync(dependencies: any[]): any[] {

    const dependenciesForFunction: any[] = [];
    for (let i: number = 0; i < dependencies.length; i++) {
      if (typeof dependencies[i] === "string") {
        const dependency = this.GetModuleSync(dependencies[i]);
        dependenciesForFunction.push(dependency);
      } else {
        dependenciesForFunction.push(dependencies[i]);
      }
    }

    return dependenciesForFunction;
  }


  /**
   * Gets dependencies asynchronously
   *
   * @private
   * @param {any[]} dependencies
   * @returns {Promise<any[]>}
   * @memberof ModuleResolver
   */
  private async GetDependencies(dependencies: any[]): Promise<any[]> {
    return new Promise<any[]>(async resolve => {
      const dependenciesForFunction: any[] = [];
      for (let i: number = 0; i < dependencies.length; i++) {
        if (typeof dependencies[i] === "string") {
          const dependency = await this.GetModule(dependencies[i]);
          dependenciesForFunction.push(dependency);
        } else {
          dependenciesForFunction.push(dependencies[i]);
        }
      }
      resolve(dependenciesForFunction);
    });
  }
}
