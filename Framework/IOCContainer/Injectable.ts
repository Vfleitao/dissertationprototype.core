import ModuleResolver from "~/Core/Framework/IOCContainer/ModuleResolver";

/**
 * Automatically starts a given class with dependecies
 * and registers it in the IOC container s it can be used
 * in dependency injection
 *
 * @export
 * @param {string} injectionKey
 * @param {Array<any>} [dependencies=[]]
 * @returns
 */
export default function Injectable(injectionKey: string, dependencies: Array<any> = []) {
  return function (target: any): void {
    ModuleResolver.Instance.RegisterModuleSync(injectionKey, dependencies, target);
  };
}
