import ModuleRunner from "~/Core/Framework/IOCContainer/ModuleRunner";


/**
 * Automatically starts a class with any dependencies
 * without any registration in the IOC container
 *
 * @export
 * @param {Array<any>} [dependencies=[]]
 * @returns
 */
export default function AutoStart(dependencies: Array<any> = []) {
  return function (target: any): void {
    ModuleRunner.Instance.RunModule(dependencies, target);
  };
}
