﻿import ModuleResolver from "~/Core/Framework/IOCContainer/ModuleResolver";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import ArrayUtils from "~/Core/Framework/Utils/ArrayUtils";


/**
 * Class responsible to start up modules and
 * the declared dependencies
 *
 * @export
 * @class ModuleRunner
 */
export default class ModuleRunner {
  public static Instance: ModuleRunner = new ModuleRunner(ModuleResolver.Instance);
     
  private moduleResolver: ModuleResolver;

  /**
   *Creates an instance of ModuleRunner.
   * @param {ModuleResolver} moduleResolver
   * @memberof ModuleRunner
   */
  constructor(moduleResolver: ModuleResolver) {
    this.moduleResolver = moduleResolver;
  }


  /**
   * Runs a module and fetches its dependencies
   *
   * @param {any[]} dependencies
   * @param {*} module
   * @returns
   * @memberof ModuleRunner
   */
  public async RunModule(dependencies: any[], module: any) {
    if (!module) {
      return;
    }
    if (dependencies && dependencies.length) {
      const dependenciesForFunction: any[] = await this.GetDependencies(dependencies);
      await this.CreateModuleInstance(module, dependenciesForFunction);
    } else {
      await this.CreateModuleInstance(module, null);
    }
  }


  /**
   * Created and returns a module with is dependencies
   *
   * @param {*} module
   * @param {any[]} dependencies
   * @returns {Promise<any>}
   * @memberof ModuleRunner
   */
  public async CreateModuleInstance(module: any, dependencies: any[]): Promise<any> {
    if (ObjectUtils.IsNullOrUndefined(module)) {
      return;
    }

    if (ArrayUtils.IsNullOrEmpty(dependencies)) {
      return new module();
    }

    return new module(... await this.GetDependencies(dependencies));
  }


  /**
   * Retrieves all dependencies for a given object
   *
   * @private
   * @param {any[]} dependencieRequest
   * @returns {Promise<any[]>}
   * @memberof ModuleRunner
   */
  private async GetDependencies(dependencieRequest: any[]): Promise<any[]> {
    const dependenciesForFunction: any[] = [];

    for (let i: number = 0; i < dependencieRequest.length; i++) {
      if (typeof dependencieRequest[i] === "string") {
        const requestedModule: any = await this.moduleResolver.GetModule(dependencieRequest[i]);
        dependenciesForFunction.push(requestedModule);
      } else {
        dependenciesForFunction.push(dependencieRequest[i]);
      }
    }

    return dependenciesForFunction;
  }
}
