import Component from "~/Core/Framework/Components/Component";


/**
 * Abstract class for any element retriever to implement
 *
 * @abstract
 * @class AbstractElementRetriever
 */
abstract class AbstractElementRetriever {
   public abstract GetNativeElement(component: Component): object;
}

export default AbstractElementRetriever;
