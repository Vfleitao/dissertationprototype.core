import Component from "~/Core/Framework/Components/Component";


/**
 * Abstact class with contract for native
 * element processors
 *
 * @abstract
 * @class AbstractNativeElementProcessor
 */
abstract class AbstractNativeElementProcessor {
    abstract ProcessComponent(component: Component);
    abstract RemoveChildFromComponent(parent: Component, child: Component);
    abstract AddChildToComponent(parent: Component, child: Component);
}

export default AbstractNativeElementProcessor;
