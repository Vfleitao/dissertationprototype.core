import Component from "~/Core/Framework/Components/Component";
import AbstractElementRetriever from "~/Core/Framework/ElementProcessor/AbstractElementRetriever";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import ArrayUtils from "~/Core/Framework/Utils/ArrayUtils";
import Injectable from "~/Core/Framework/IOCContainer/Injectable";
import AbstractNativeElementProcessor from "~/Core/Framework/ElementProcessor/AbstractNativeElementProcessor";

/**
 * Element Processor class responsible for using
 * Element Retrievers and attaching childs
 * in parents
 *
 * @export
 * @class ElementProcessor
 */
@Injectable("ElementProcessor")
export default class ElementProcessor {

    /**
     * Map of component types
     * to retrievers
     *
     * @private
     * @type {Array<{
     *         componentType: string,
     *         elementRetriever: AbstractElementRetriever
     *     }>}
     * @memberof ElementProcessor
     */
    private elementRetrieverMapping: Array<{
        componentType: string,
        elementRetriever: AbstractElementRetriever
    }> = [];


    /**
     * Native Element Processor responsible
     * for deciding how children are introduced in parents
     * native objects
     *
     * @private
     * @type {AbstractNativeElementProcessor}
     * @memberof ElementProcessor
     */
    private nativeElementProcessor: AbstractNativeElementProcessor;


    /**
     * Adding a new Element Retriever
     *
     * @param {*} componentClass
     * @param {AbstractElementRetriever} elementRetriever
     * @memberof ElementProcessor
     */
    public AddElementRetriever(componentClass: any, elementRetriever: AbstractElementRetriever): void {
        if (ObjectUtils.IsNullOrUndefined(componentClass) || ObjectUtils.IsNullOrUndefined(elementRetriever)) {
            throw new Error("Invalid Arguments");
        }

        this.elementRetrieverMapping.push({
            componentType: componentClass.name,
            elementRetriever: elementRetriever
        });
    }


    /**
     * Adding a new element retriever to multiple
     * component types
     *
     * @param {any[]} [componentClasses=[]]
     * @param {AbstractElementRetriever} elementRetriever
     * @memberof ElementProcessor
     */
    public AddElementRetrievers(componentClasses: any[] = [], elementRetriever: AbstractElementRetriever) {
        for (let i: number = 0, ln = ArrayUtils.GetLength(componentClasses); i < ln; i++) {
            this.AddElementRetriever(componentClasses[i], elementRetriever);
        }
    }


    /**
     * Set the native element processor
     *
     * @param {AbstractNativeElementProcessor} nativeElementProcessor
     * @memberof ElementProcessor
     */
    public SetNativeElementProcessor(nativeElementProcessor: AbstractNativeElementProcessor) {
        this.nativeElementProcessor = nativeElementProcessor;
    }


    /**
     * Triggers the process of parsing the
     * component and creating the native view
     * using Element Retrievers and the 
     * native element processor
     *
     * @param {Component} component
     * @memberof ElementProcessor
     */
    public ProcessComponent(component: Component): void {
        if (ObjectUtils.IsNullOrUndefined(component)) {
            throw new Error("Invalid Arguments");
        }

        this.HandleNativeElements(component);
        this.nativeElementProcessor.ProcessComponent(component);
    }


    /**
     * Add a child to a parent component
     *
     * @param {Component} parent
     * @param {Component} child
     * @returns
     * @memberof ElementProcessor
     */
    public AddChildToComponent(parent: Component, child: Component) {
        if (ObjectUtils.IsNullOrUndefined(parent) ||
            ObjectUtils.IsNullOrUndefined(child)) {
            throw new Error("Invalid Arguments");
        }

        if (ObjectUtils.IsNullOrUndefined(parent.NativeElement)) {
            return;
        }

        if (ObjectUtils.IsNullOrUndefined(child.NativeElement)) {
            this.HandleNativeElements(child);
        }

        this.nativeElementProcessor.AddChildToComponent(parent, child);
    }
7
    /**
     * Removes a child form a parent component
     *
     * @param {Component} parent
     * @param {Component} child
     * @returns
     * @memberof ElementProcessor
     */
    public RemoveChildFromComponent(parent: Component, child: Component) {
        if (ObjectUtils.IsNullOrUndefined(parent) ||
            ObjectUtils.IsNullOrUndefined(child)) {
            throw new Error("Invalid Arguments");
            return;
        }

        if (ObjectUtils.IsNullOrUndefined(parent.NativeElement) ||
            ObjectUtils.IsNullOrUndefined(child.NativeElement)) {
            return;
        }

        this.nativeElementProcessor.RemoveChildFromComponent(parent, child);
    }

    /**
     * Loop parent and children and retrieve the native
     * element for them
     *
     * @private
     * @param {Component} component
     * @returns
     * @memberof ElementProcessor
     */
    private HandleNativeElements(component: Component) {
        if (ObjectUtils.IsNullOrUndefined(component.NativeElement)) {
            const retriever: AbstractElementRetriever = this.GetRetriever(component);
            if (ObjectUtils.IsNullOrUndefined(retriever)) {
                return;
            }

            component.NativeElement = retriever.GetNativeElement(component);
        }

        for (let i: number = 0, ln = ArrayUtils.GetLength(component.Children); i < ln; i++) {
            this.HandleNativeElements(component.Children[i]);
        }
    }


    /**
     * Get an element retriever for a given
     * component
     *
     * @private
     * @param {Component} component
     * @returns {*}
     * @memberof ElementProcessor
     */
    private GetRetriever(component: Component): any {
        for (let i: number = 0, ln = ArrayUtils.GetLength(this.elementRetrieverMapping); i < ln; i++) {
            if (this.elementRetrieverMapping[i].componentType === component.constructor.name) {
                return this.elementRetrieverMapping[i].elementRetriever;
            }
        }
        return null;
    }
}

