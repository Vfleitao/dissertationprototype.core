import Component from "~/Core/Framework/Components/Component";
import IInitialLabelProperties from "./Properties/IInitialLabelProperties";
import ObjectUtils from "../Utils/ObjectUtils";

/**
 * Label component for displaying text
 *
 * @export
 * @class LabelComponent
 * @extends {Component}
 */
export default class LabelComponent extends Component {
  private text: string;

  /**
   * Gets the text
   *
   * @type {string}
   * @memberof LabelComponent
   */
  public get Text(): string {
    return this.text;
  }

   /**
    * Sets the text
    *
    * @memberof LabelComponent
   */
  public set Text(value: string) {
    this.text = value;
    this.textSetter.SetText(this.Text, this);
  }

  /**
   * Creates an instance of LabelComponent.
   * @param {IInitialLabelProperties} initialProperties
   * @memberof LabelComponent
  */
  constructor(initialProperties?: IInitialLabelProperties) {
    super(initialProperties);

    if (!ObjectUtils.IsNullOrUndefined(initialProperties)) {
      this.text = initialProperties.InitialText;
    }
  }

  /**
   * On Native element set
   *
   * @protected
   * @param {object} nativeElement
   * @memberof LabelComponent
   */
  protected OnNativeElementSet(nativeElement: object): void {
    super.OnNativeElementSet(nativeElement);
    this.textSetter.SetText(this.text, this);
  }
}
