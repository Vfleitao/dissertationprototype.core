import InputComponent from "~/Core/Framework/Components/InputComponent";
import IInitialInputProperties from "~/Core/Framework/Components/Properties/IInitialInputProperties";

/**
 * Input which must take the form of a multi line
 * input component
 *
 * @export
 * @class MultilineInputComponent
 * @extends {InputComponent}
 */
export default class MultilineInputComponent extends InputComponent {
    constructor(initialProperties?: IInitialInputProperties) {
        super(initialProperties);
    }
}