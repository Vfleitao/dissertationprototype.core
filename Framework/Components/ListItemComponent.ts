import ContainerComponent from "~/Core/Framework/Components/ContainerComponent";
import IContainerProperties from "~/Core/Framework/Components/Properties/IContainerProperties";

import Component from "~/Core/Framework/Components/Component";

/**
 * List Item component
 * used to display component inside a list
 *
 * @export
 * @abstract
 * @class ListItemComponent
 * @extends {Component}
 * @template T
 */
export default abstract class ListItemComponent<T> extends Component {
    private data: T;

    /**
     * Data Getter
     *
     * @type {T}
     * @memberof ListItemComponent
     */
    public get Data(): T {
        return this.data;
    }

    /**
     * Data Setter
     *
     * @memberof ListItemComponent
     */
    public set Data(value: T) {
        this.data = value;

        this.OnDataSet();
    }

    /**
     * Gets a component Id
     *
     * @readonly
     * @abstract
     * @type {string}
     * @memberof ListItemComponent
     */
    public abstract get Id(): string;

        /**
     *Creates an instance of ListItemComponent.
     * @param {T} data
     * @param {IContainerProperties} initialProperties
     * @memberof ListItemComponent
     */
    constructor(data: T, initialProperties?: IContainerProperties) {
        super(initialProperties);

        this.data = data;

        this.OnInitialize();
    }

    /**
     * On Data Set
     *
     * @protected
     * @abstract
     * @memberof ListItemComponent
     */
    protected abstract OnDataSet();
    
    /**
     * On Initialize
     *
     * @protected
     * @abstract
     * @memberof ListItemComponent
     */
    protected abstract OnInitialize();
}