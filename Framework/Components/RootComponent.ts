import Component from "~/Core/Framework/Components/Component";

/**
 * Root component which will house the
 * application
 *
 * @export
 * @class RootComponent
 * @extends {Component}
 */
export default class RootComponent extends Component {
    constructor() {
        super();
    }
}