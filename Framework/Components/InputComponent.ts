import Component from "~/Core/Framework/Components/Component";
import IInitialInputProperties from "~/Core/Framework/Components/Properties/IInitialInputProperties";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";

/**
 * Input component used for text input
 *
 * @export
 * @class InputComponent
 * @extends {Component}
 */
export default class InputComponent extends Component {
    private initialValue: string;

    /**
     * Gets the current value
     *
     * @type {string}
     * @memberof InputComponent
     */
    public get Value(): string {
        return this.textGetter.GetText(this);
    }

    /**
     * Setting the current value
     *
     * @memberof InputComponent
     */
    public set Value(value: string) {
        this.textSetter.SetText(value, this);
    }

     /**
     *Creates an instance of InputComponent.
     * @param {IInitialInputProperties} initialProperties
     * @memberof InputComponent
     */
    constructor(initialProperties?: IInitialInputProperties) {
        super(initialProperties);

        if (!ObjectUtils.IsNullOrUndefined(initialProperties)) {
            this.initialValue = initialProperties.InitialValue;
        }
    }

    /**
     * On native element set
     *
     * @protected
     * @param {object} nativeElement
     * @memberof InputComponent
     */
    protected OnNativeElementSet(nativeElement: object): void {
        super.OnNativeElementSet(nativeElement);
        this.textSetter.SetText(this.initialValue, this);
    }
}
