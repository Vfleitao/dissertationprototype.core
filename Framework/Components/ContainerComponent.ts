import Component from "~/Core/Framework/Components/Component";
import Orientation from "~/Core/Framework/Enums/Orientation";
import IContainerProperties from "~/Core/Framework/Components/Properties/IContainerProperties";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";


/**
 * Container component
 *
 * @export
 * @class ContainerComponent
 * @extends {Component}
 */
export default class ContainerComponent extends Component {
    private orientation: Orientation;

    /**
     * Get the container orientation
     *
     * @memberof ContainerComponent
     */
    public get Orientation() {
        return this.orientation;
    }

    /**
     * Set container orientation
     *
     * @memberof ContainerComponent
     */
    public set Orientation(value: Orientation) {
        this.orientation = value;
    }
     /**
     * Creates an instance of ContainerComponent.
     * @param {IContainerProperties} initialProperties
     * @memberof ContainerComponent
     */
    constructor(initialProperties?: IContainerProperties) {
        super(initialProperties);

        if (ObjectUtils.IsNullOrUndefined(initialProperties)) {
            this.orientation = Orientation.Horizontal;
        } else {
            this.orientation = initialProperties.Orientation;
        }
    }
}
