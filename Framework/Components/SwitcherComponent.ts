import Component from "~/Core/Framework/Components/Component";
import EventType from "~/Core/Framework/Enums/EventType";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import AbstractSwitcherValueSetter from "~/Core/Framework/Appearance/AbstractSwitcherValueSetter";
import ModuleResolver from "~/Core/Framework/IOCContainer/ModuleResolver";
import IInitialSwitcherProperties from "./Properties/IInitialSwitcherProperties";


/**
 * Switcher component for
 * On/Off data
 *
 * @export
 * @class SwitcherComponent
 * @extends {Component}
 */
export default class SwitcherComponent extends Component {
    private switcherValueSetter: AbstractSwitcherValueSetter;
    private on: boolean;
    
    /**
     * On State Change handler
     *
     * @memberof SwitcherComponent
     */
    public OnStateChange: ((value: boolean) => void | Function) = null;

    /**
     * Gets On
     *
     * @type {boolean}
     * @memberof SwitcherComponent
     */
    public get On(): boolean {
        return this.on;
    }

    /**
     * Sets On
     *
     * @memberof SwitcherComponent
     */
    public set On(value: boolean) {
        this.on = value;

        this.switcherValueSetter.SetValue(this.On, this);
    }

     /**
     * Creates an instance of SwitcherComponent.
     * @param {IInitialSwitcherProperties} initialProperties
     * @memberof SwitcherComponent
     */
    constructor(initialProperties: IInitialSwitcherProperties) {
        super(initialProperties);
        this.switcherValueSetter = ModuleResolver.Instance.GetModuleSync(
            "SwitcherValueSetter"
        );

        if (ObjectUtils.IsNullOrUndefined(initialProperties)) {
            this.On = false;
        } else {
            this.On = initialProperties.On || false;
        }
    }


    /**
     * On Native element set handler
     *
     * @protected
     * @param {object} nativeElement
     * @memberof SwitcherComponent
     */
    protected OnNativeElementSet(nativeElement: object): void {
        super.OnNativeElementSet(nativeElement);
        this.eventProcessor.RegisterEvent(
            EventType.SwitchStateChange,
            this,
            this.OnSwichStateChanged.bind(this)
        );
    }


    /**
     * private state handler
     *
     * @private
     * @param {boolean} newValue
     * @returns
     * @memberof SwitcherComponent
     */
    private OnSwichStateChanged(newValue: boolean) {
        this.on = newValue;

        if (ObjectUtils.IsNullOrUndefined(this.OnStateChange)) {
            return;
        }

        this.OnStateChange(newValue);
    }
}
