import IInitialProperties from "~/Core/Framework/Components/Properties/IInitialProperties";

export default interface IInitialInputProperties extends IInitialProperties {
    InitialValue?: string;
}
