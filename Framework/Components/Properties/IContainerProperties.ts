import IInitialProperties from "~/Core/Framework/Components/Properties/IInitialProperties";
import Orientation from "../../Enums/Orientation";

export default interface IContainerProperties extends IInitialProperties {
    Orientation?: Orientation;
}
