import IInitialProperties from "~/Core/Framework/Components/Properties/IInitialProperties";

export default interface IInitialSwitcherProperties extends IInitialProperties {
    On?: boolean;
}
