import IInitialLabelProperties from "~/Core/Framework/Components/Properties/IInitialLabelProperties";

export default interface IInitialButtonProperties extends IInitialLabelProperties {}
