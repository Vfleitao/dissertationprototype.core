import IInitialProperties from "~/Core/Framework/Components/Properties/IInitialProperties";

export default interface IInitialLabelProperties extends IInitialProperties {
  InitialText: string;
}
