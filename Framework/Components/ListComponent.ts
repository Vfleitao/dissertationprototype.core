import ListItemComponent from "~/Core/Framework/Components/ListItemComponent";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import ArrayUtils from "~/Core/Framework/Utils/ArrayUtils";
import ContainerComponent from "~/Core/Framework/Components/ContainerComponent";
import IContainerProperties from "~/Core/Framework/Components/Properties/IContainerProperties";


/**
 * List component used to create new 
 * list items
 *
 * @export
 * @abstract
 * @class ListComponent
 * @extends {ContainerComponent}
 * @template T
 * @template E
 */
export default abstract class ListComponent<T, E extends ListItemComponent<T>> extends ContainerComponent {
    
    /**
     * Data
     *
     * @private
     * @type {T[]}
     * @memberof ListComponent
     */
    private data: T[] = [];
    
    /**
     * Items
     *
     * @private
     * @type {{ [id: string]: E }}
     * @memberof ListComponent
     */
    private items: { [id: string]: E } = {};

    /**
     * Data Getter
     *
     * @type {T[]}
     * @memberof ListComponent
     */
    public get Data(): T[] {
        return this.data;
    }

    /**
     * Data Setter
     *
     * @memberof ListComponent
     */
    public set Data(value: T[]) {
        this.data = value;
    }

    /**
     * Creates an instance of ListComponent.
     * @param {T[]} [data]
     * @param {IContainerProperties} initialProperties
     * @memberof ListComponent
     */
    constructor(data?: T[], initialProperties?: IContainerProperties) {
        super(initialProperties);

        if (!ArrayUtils.IsNullOrEmpty(data)) {
            this.data = data;
        }

        this.OnInitialize();
    }

    /**
     * Gets the id of a data item
     *
     * @protected
     * @abstract
     * @param {T} data
     * @returns {string}
     * @memberof ListComponent
     */
    protected abstract GetId(data: T): string;

    /**
     * Creates a new component instance
     *
     * @protected
     * @abstract
     * @param {T} data
     * @returns {E}
     * @memberof ListComponent
     */
    protected abstract CreateNewComponent(data: T): E;

    /**
     * On Component initialize
     *
     * @protected
     * @returns
     * @memberof ListComponent
     */
    protected OnInitialize() {
        if (ArrayUtils.IsNullOrEmpty(this.Data)) {
            return;
        }

        this.Data.forEach((e) => {
            const newItem = this.CreateNewComponent(e);
            this.items[this.GetId(e)] = newItem;
            this.AddChild(newItem);
        });
    }


    /**
     * Gets a child component
     *
     * @protected
     * @param {T} data
     * @returns {E}
     * @memberof ListComponent
     */
    protected GetComponent(data: T): E {
        if (ObjectUtils.IsNullOrUndefined(this.GetId(data)) ||
            ObjectUtils.IsNullOrUndefined(this.items[this.GetId(data)])) {
            return null;
        }

        return this.items[this.GetId(data)];
    }

    /**
     * Adds a new item to the list
     *
     * @param {T} item
     * @returns
     * @memberof ListComponent
     */
    public AddNewItem(item: T) {
        if (ObjectUtils.IsNullOrUndefined(item)) {
            return;
        }

        this.data.push(item);
        const newItem = this.CreateNewComponent(item);
        this.items[this.GetId(item)] = newItem;

        this.AddChild(newItem);

        if (ObjectUtils.IsNullOrUndefined(this.NativeElement)) {
            return;
        }

        this.UpdateComponent();
    }

    /**
     * Removes an item from the list
     *
     * @param {T} item
     * @returns
     * @memberof ListComponent
     */
    public RemoveItem(item: T) {
        if (ObjectUtils.IsNullOrUndefined(item) ||
            ArrayUtils.IsNullOrEmpty(this.Data)) {
            return;
        }

        const id: string = this.GetId(item);

        let indexToRemove: number = 0;
        let found: boolean = false;
        let counter: number = 0;
        let itemToRemove: T = null

        this.Data.forEach((e: T) => {
            if (this.GetId(e) === id) {
                found = true;
                indexToRemove = counter;
                itemToRemove = e;
            }

            counter++;
        });

        if (!found) {
            return;
        }

        this.Data.splice(indexToRemove, 1);
        this.RemoveChild(this.items[this.GetId(itemToRemove)]);
        delete this.items[this.GetId(itemToRemove)];
    }
}
