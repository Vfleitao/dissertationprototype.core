import Component from "~/Core/Framework/Components/Component";
import IInitialProperties from "~/Core/Framework/Components/Properties/IInitialProperties";

/**
 * Form component base
 *
 * @export
 * @class FormComponent
 * @extends {Component}
 */
export default class FormComponent extends Component {

    /**
     * Creates an instance of FormComponent.
     * @param {IInitialProperties} [initialProperties]
     * @memberof FormComponent
     */
    constructor(initialProperties?: IInitialProperties) {
        super(initialProperties);
    }

    /**
     * Serialises the form content into
     * an object
     *
     * @protected
     * @returns {*}
     * @memberof FormComponent
    */
    protected Serialize(): any {
        return null;
    }
}