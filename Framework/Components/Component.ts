import NotificationService from "~/Core/Framework/NotificationService/NotificationService";
import AbstractAppearanceSetter from "~/Core/Framework/Appearance/AbstractAppearanceSetter";
import AbstractTextSetter from "~/Core/Framework/Appearance/AbstractTextSetter";
import RandomKeyGenerator from "~/Core/Framework/Utils/RandomKeyGenerator";
import ModuleResolver from "~/Core/Framework/IOCContainer/ModuleResolver";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import AbstractTextGetter from "~/Core/Framework/Appearance/AbstractTextGetter";
import EventProcessor from "~/Core/Framework/Events/EventProcessor";
import ElementProcessor from "~/Core/Framework/ElementProcessor/ElementProcessor";
import EventType from "~/Core/Framework/Enums/EventType";
import IInitialProperties from "~/Core/Framework/Components/Properties/IInitialProperties";


/**
 * Generic class for components to inherit from
 * provides all common functionality present in
 * all components in the app
 *
 * @export
 * @class Component
 */
export default class Component {

    /**
     * Text setter
     *
     * @protected
     * @type {AbstractTextSetter}
     * @memberof Component
     */
    protected textSetter: AbstractTextSetter;

    /**
     * Text Getter
     *
     * @protected
     * @type {AbstractTextGetter}
     * @memberof Component
     */
    protected textGetter: AbstractTextGetter;

    /**
     * Appearance setter
     *
     * @protected
     * @type {AbstractAppearanceSetter}
     * @memberof Component
     */
    protected appearanceSetter: AbstractAppearanceSetter;

    /**
     * Notification Service
     *
     * @protected
     * @type {NotificationService}
     * @memberof Component
     */
    protected notificationService: NotificationService;

    /**
     * Event Processor
     *
     * @protected
     * @type {EventProcessor}
     * @memberof Component
     */
    protected eventProcessor: EventProcessor;

    /**
     * Element Processor
     *
     * @protected
     * @type {ElementProcessor}
     * @memberof Component
     */
    protected elementProcessor: ElementProcessor;

    private id: string;
    private nativeElement: object;
    private events: { [id: number]: ((value?: any) => void | Function)[] } = []


    /**
     * Children for the Component
     *
     * @type {Component[]}
     * @memberof Component
     */
    public Children: Component[] = [];
    appearance: string;


    /**
     * Native element getter
     *
     * @type {object}
     * @memberof Component
     */
    public get NativeElement(): object {
        return this.nativeElement;
    }


    /**
     * Native Element Setter
     *
     * @memberof Component
     */
    public set NativeElement(value: object) {
        this.nativeElement = value;

        this.OnNativeElementSet(this.nativeElement);
    }


    /**
     * Gets the components appearance
     *
     * @readonly
     * @protected
     * @type {AbstractAppearanceSetter}
     * @memberof Component
     */
    protected get AppearanceSetter(): AbstractAppearanceSetter {
        return this.appearanceSetter;
    }


    /**
     * Gets the notification service
     *
     * @readonly
     * @protected
     * @type {NotificationService}
     * @memberof Component
     */
    protected get NotificationService(): NotificationService {
        return this.notificationService;
    }


    /**
     * Gets the component Id
     *
     * @readonly
     * @type {string}
     * @memberof Component
     */
    public get Id(): string {
        return this.id;
    }

    public get Appearance(): string {
        return this.appearance;
    }

     /**
     * Creates an instance of Component.
     * @param {IInitialProperties} initialProperties
     * @memberof Component
     */
    constructor(initialProperties?: IInitialProperties) {
        this.id = RandomKeyGenerator.GenerateKey();

        // Due to how module loading works in javascript
        // its safer to implement the module resolution at this level rather than
        // use the @Inject option        
        this.notificationService = ModuleResolver.Instance.GetModuleSync("NotificationService");
        this.appearanceSetter = ModuleResolver.Instance.GetModuleSync("AppearanceSetter");
        this.textSetter = ModuleResolver.Instance.GetModuleSync("TextSetter");
        this.textGetter = ModuleResolver.Instance.GetModuleSync("TextGetter");
        this.eventProcessor = ModuleResolver.Instance.GetModuleSync("EventProcessor");
        this.elementProcessor = ModuleResolver.Instance.GetModuleSync("ElementProcessor");

        if (!ObjectUtils.IsNullOrUndefined(initialProperties)) {
            this.SetInitialProperties(initialProperties);
        }

        this.AddNotificationHandlers();
    }
    
    /**
     * Sets the initial properties of a component
     * such as appearance
     *
     * @private
     * @param {IInitialProperties} initialProperties
     * @memberof Component
     */
    private SetInitialProperties(initialProperties: IInitialProperties) {
        this.appearance = initialProperties.Appearance;
    }

    /**
     *Adds a new child to the component
     *
     * @param {Component} child
     * @returns
     * @memberof Component
     */
    public AddChild(child: Component) {
        this.Children.push(child);

        if (ObjectUtils.IsNullOrUndefined(this.nativeElement)) {
            return;
        }

        this.elementProcessor.AddChildToComponent(this, child);
    }


    /**
     * Removes a child from the component
     *
     * @param {Component} child
     * @returns
     * @memberof Component
     */
    public RemoveChild(child: Component) {
        if (ObjectUtils.IsNullOrUndefined(child)
            || !this.nativeElement) {
            return;
        }

        this.elementProcessor.RemoveChildFromComponent(this, child);

        const index = this.Children.indexOf(child);
        this.Children.splice(index, 1);
    }

    /**
     * Sets the appearance of the component
     *
     * @param {string} name
     * @memberof Component
     */
    public SetAppearance(name: string) {
        this.appearance = name;
        this.AppearanceSetter.SetAppearance(name, this);
    }

    /**
     * Updates the current component
     *
     * @memberof Component
     */
    public UpdateComponent() {
        this.elementProcessor.ProcessComponent(this);
    }

    /**
     * Register a new event
     *
     * @param {EventType} type
     * @param {((value?: any) => void | Function)} handler
     * @returns
     * @memberof Component
     */
    public RegisterEvent(type: EventType, handler: (value?: any) => void | Function) {
        if (ObjectUtils.IsNullOrUndefined(this.events[type])) {
            this.events[type] = [];
        }

        this.events[type].push(handler);

        if (ObjectUtils.IsNullOrUndefined(this.NativeElement)) {
            return;
        }

        this.eventProcessor.RegisterEvent(type, this, handler);
    }


    /**
     * On On Native Element Set
     *
     * @protected
     * @param {object} nativeElement
     * @memberof Component
     */
    protected OnNativeElementSet(nativeElement: object): void {
        for (let type in EventType) {
            const evt: EventType = type as any;
            if (!ObjectUtils.IsNullOrUndefined(this.events[evt])) {
                this.events[evt].forEach(handler => {
                    this.eventProcessor.RegisterEvent(evt, this, handler);
                });
            }
        }
    }


    /**
     * Send Notification
     *
     * @protected
     * @param {string} name
     * @param {*} [body]
     * @param {*} [senderKey]
     * @returns
     * @memberof Component
     */
    protected SendNotification(name: string, body?: any, senderKey?: any) {
        if (ObjectUtils.IsNullOrUndefined(name)) {
            return;
        }

        this.notificationService.SendNotification(name, body, senderKey);
    }

    /**
     * Adding Notification Handlers
     * Any registration for event handlers should be done here
     *
     * @protected
     * @memberof Component
     */
    protected AddNotificationHandlers() { }


    /**
     * Registers a new notification
     *
     * @protected
     * @param {string} notificationName
     * @param {(name: string, body?: any, senderKey?: any) => void} handler
     * @returns
     * @memberof Component
     */
    protected RegisterNotification(notificationName: string, handler: (name: string, body?: any, senderKey?: any) => void) {
        if (ObjectUtils.IsNullOrUndefined(notificationName) ||
            ObjectUtils.IsNullOrUndefined(handler)) {
            return;
        }

        this.notificationService.Register({
            NotificationName: notificationName,
            NotificationHandler: handler,
            Context: {
                ContextKey: this.Id
            }
        });
    }


    /**
     * Unregisters a new notification
     *
     * @protected
     * @param {string} notificationName
     * @param {(name: string, body?: any, senderKey?: any) => void} handler
     * @returns
     * @memberof Component
     */
    protected UnregisterNotification(notificationName: string, handler: (name: string, body?: any, senderKey?: any) => void) {
        if (ObjectUtils.IsNullOrUndefined(notificationName) ||
            ObjectUtils.IsNullOrUndefined(handler)) {
            return;
        }

        this.notificationService.UnRegister({
            NotificationName: notificationName,
            NotificationHandler: handler,
            Context: {
                ContextKey: this.Id
            }
        });
    }
}