import Component from "~/Core/Framework/Components/Component";
import EventType from "~/Core/Framework/Enums/EventType";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import IInitialButtonProperties from "~/Core/Framework/Components/Properties/IInitialButtonProperties";


/**
 * Button Component class
 *
 * @export
 * @class ButtonComponent
 * @extends {Component}
 */
export default class ButtonComponent extends Component {
    private text: string;

    /**
     * OnClick funcion called when the button is clicked
     *
     * @type {Function}
     * @memberof ButtonComponent
     */
    public OnClick: Function = null;

    /**
     * Gets a buttons text
     *
     * @type {string}
     * @memberof ButtonComponent
     */
    public get Value(): string {
        return this.text;
    }

    /**
     * Setting a buttons text
     *
     * @memberof ButtonComponent
     */
    public set Value(value: string) {
        this.text = value;
        this.textSetter.SetText(this.text, this);
    }

    /**
     *Creates an instance of ButtonComponent.
     * @param {IInitialButtonProperties} initialProperties
     * @memberof ButtonComponent
     */
    constructor(initialProperties?: IInitialButtonProperties) {
        super(initialProperties);

        if (!ObjectUtils.IsNullOrUndefined(initialProperties)) {
            this.text = initialProperties.InitialText;
        }
    }

    /**
     * On Native Element Set
     *
     * @protected
     * @param {object} nativeElement
     * @memberof ButtonComponent
     */
    protected OnNativeElementSet(nativeElement: object): void {
        super.OnNativeElementSet(nativeElement);
        this.textSetter.SetText(this.text, this);
        this.eventProcessor.RegisterEvent(
            EventType.Click,
            this,
            this.OnClickTriggered.bind(this)
        );
    }

    /**
     * Button clicked internal handler
     *
     * @private
     * @returns
     * @memberof ButtonComponent
     */
    private OnClickTriggered() {
        if (ObjectUtils.IsNullOrUndefined(this.OnClick)) {
            return;
        }

        this.OnClick();
    }
}
