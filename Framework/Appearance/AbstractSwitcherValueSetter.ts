import Component from "~/Core/Framework/Components/Component";

/**
 * Abstract class for switcher value
 * setter
 *
 * @abstract
 * @class AbstractSwitcherValueSetter
 */
abstract class AbstractSwitcherValueSetter {
    public abstract SetValue(on: boolean, component: Component): void;
}

export default AbstractSwitcherValueSetter;
