import Component from "~/Core/Framework/Components/Component";

/**
 * Abstract class for text getter
 *
 * @abstract
 * @class AbstractTextGetter
 */
abstract class AbstractTextGetter {
    public abstract GetText(component: Component): string;
}

export default AbstractTextGetter;
