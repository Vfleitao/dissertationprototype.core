import Component from "~/Core/Framework/Components/Component";

/**
 * Abstract class for appearance setter
 *
 * @abstract
 * @class AbstractAppearanceSetter
 */
abstract class AbstractAppearanceSetter {
   public abstract SetAppearance(name: string, component: Component): void;
}

export default AbstractAppearanceSetter;
