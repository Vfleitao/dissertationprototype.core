import Component from "~/Core/Framework/Components/Component";


/**
 * Abstract class for text getter
 *
 * @abstract
 * @class AbstractTextSetter
 */
abstract class AbstractTextSetter {
    public abstract SetText(text: string, component: Component): void;
}

export default AbstractTextSetter;
