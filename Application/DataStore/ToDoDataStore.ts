import NotificationService from "~/Core/Framework/NotificationService/NotificationService";
import DataStore from "~/Core/Framework/DataStore/DataStore";
import ToDoModel from "~/Core/Application/Models/ToDoModel";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import Injectable from "~/Core/Framework/IOCContainer/Injectable";
import Notifications from "~/Core/Application/Consts/Notifications";

/**
 * To Do Data Stpre
 *
 * @export
 * @class ToDoDataStore
 * @extends {DataStore<ToDoModel>}
 */
@Injectable("ToDoDataStore", ["NotificationService"])
export default class ToDoDataStore extends DataStore<ToDoModel> {

    protected get DataStoreItemAddedNotification(): string {
        return Notifications.TODO_ITEM_ADDED;
    }

    protected get DataStoreItemRemovedNotification(): string {
        return Notifications.TODO_ITEM_REMOVED;
    }

    protected get DataStoreItemsUpdatedNotification(): string {
        return Notifications.TODO_ITEMS_UPDATED;
    }

    /**
     * Creates an instance of ToDoDataStore.
     * @param {NotificationService} notificationService
     * @memberof ToDoDataStore
     */
    constructor(notificationService: NotificationService) {
        super(notificationService);
    }

    /**
     * Gets a ToDoModel by id
     *
     * @param {string} id
     * @returns {ToDoModel}
     * @memberof ToDoDataStore
     */
    public GetById(id: string): ToDoModel {
        for (let i = 0; i < this.Data.length; i++) {
            if (this.GetIdForItem(this.Data[i]) === id) {
                return this.Data[i];
            }
        }

        return null;
    }

    /**
     * Gets the Id of a ToDo Model
     *
     * @protected
     * @param {ToDoModel} item
     * @returns {string}
     * @memberof ToDoDataStore
     */
    protected GetIdForItem(item: ToDoModel): string {
        if (ObjectUtils.IsNullOrUndefined(item)) {
            return null;
        }

        return item.Id;
    }
}