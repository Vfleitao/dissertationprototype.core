import ModuleResolver from "~/Core/Framework/IOCContainer/ModuleResolver";
import NotificationService from "~/Core/Framework/NotificationService/NotificationService";
import ElementProcessor from "~/Core/Framework/ElementProcessor/ElementProcessor";
import MainPage from "~/Core/Application/MainPage";
import EventProcessor from "~/Core/Framework/Events/EventProcessor";
import ToDoService from "~/Core/Application/Services/ToDoService";

/**
 * Abstract application for start up
 *
 * @abstract
 * @class AbstractApplication
 */
abstract class AbstractApplication {
    protected readonly notificationService: NotificationService;
    protected readonly elementProcessor: ElementProcessor;
    protected readonly eventProcessor: EventProcessor;

    /**
     * Getting a module Resolver
     *
     * @readonly
     * @protected
     * @type {ModuleResolver}
     * @memberof AbstractApplication
     */
    protected get ModuleResolver(): ModuleResolver {
        return ModuleResolver.Instance;
    }

    /**
     *Creates an instance of AbstractApplication.
     * @param {ElementProcessor} elementProcessor
     * @param {NotificationService} readonotificationService
     * @param {EventProcessor} eventProcessor
     * @memberof AbstractApplication
     */
    constructor(
        elementProcessor: ElementProcessor,
        readonotificationService: NotificationService,
        eventProcessor: EventProcessor) {
        this.elementProcessor = elementProcessor;
        this.notificationService = readonotificationService;
        this.eventProcessor = eventProcessor;
    }

    /**
     * Application Initializing
     *
     * @protected
     * @memberof AbstractApplication
     */
    protected Initialize() {
        this.ConfigureServices();
        this.ConfigureNativeServices();
        this.StartupApplication();
    }

    /**
     * Configures all services
     *
     * @protected
     * @memberof AbstractApplication
     */
    protected ConfigureServices() {
        // Typescript requires a use of a class in order to download a module
        // if an import is unused it is removed in the compilation process
        typeof (ToDoService);
    }

    /**
     * Native Service Configuration
     *
     * @protected
     * @abstract
     * @memberof AbstractApplication
     */
    protected abstract ConfigureNativeServices();

    /**
     * Creates main page component and processes it
     *
     * @protected
     * @memberof AbstractApplication
     */
    protected StartupApplication() {
        const page: MainPage = new MainPage();
        this.elementProcessor.ProcessComponent(page.PageRoot);
    }
}

export default AbstractApplication;
