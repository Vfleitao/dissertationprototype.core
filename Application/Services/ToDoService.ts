import AutoStart from "~/Core/Framework/IOCContainer/AutoStart";
import Service from "~/Core/Framework/Service/Service";
import NotificationService from "~/Core/Framework/NotificationService/NotificationService";
import ToDoDataStore from "~/Core/Application/DataStore/ToDoDataStore";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import ToDoModel from "~/Core/Application/Models/ToDoModel";
import Notifications from "~/Core/Application/Consts/Notifications";

/**
 * ToDo Service responsible for adding and removing
 * Items from the store
 *
 * @export
 * @class ToDoService
 * @extends {Service}
 */
@AutoStart(["NotificationService", "ToDoDataStore"])
export default class ToDoService extends Service {
    
    /**
     * Since this is a in memory only
     * We add a "fake" id
     *
     * @private
     * @static
     * @type {number}
     * @memberof ToDoService
     */
    private static toDoModelId: number = 0;

    private dataStore: ToDoDataStore;

    /**
     *Creates an instance of ToDoService.
     * @param {NotificationService} notificationService
     * @param {ToDoDataStore} dataStore
     * @memberof ToDoService
     */
    constructor(notificationService: NotificationService, dataStore: ToDoDataStore) {
        super(notificationService);
        this.dataStore = dataStore;
    }

    /**
     * Setting up all notifications
     *
     * @protected
     * @memberof ToDoService
     */
    protected AddNotificationHandlers() {
        super.AddNotificationHandlers();

        this.RegisterNotification(Notifications.ADD_NEW_TODO, this.AddNewTodo.bind(this));
        this.RegisterNotification(Notifications.REMOVE_TODO, this.RemoveTodo.bind(this));
    }

    /**
     * Adds a new item in the store
     *
     * @private
     * @param {string} notificationName
     * @param {*} body
     * @param {*} [senderKey]
     * @memberof ToDoService
     */
    private async AddNewTodo(notificationName: string, body: any, senderKey?: any) {
        if (!ObjectUtils.IsNullOrUndefined(body)) {
            const toDoModel: ToDoModel = body as ToDoModel;
            toDoModel.Id = (ToDoService.toDoModelId++).toString();
            this.dataStore.Add(toDoModel);
        }
    }

    /**
     * Removed an item from the store
     *
     * @private
     * @param {string} notificationName
     * @param {*} body
     * @param {*} [senderKey]
     * @memberof ToDoService
     */
    private async RemoveTodo(notificationName: string, body: any, senderKey?: any) {
        if (!ObjectUtils.IsNullOrUndefined(body)) {
            const toDoModel: ToDoModel = body as ToDoModel;
            this.dataStore.Remove(toDoModel);
        }
    }
}