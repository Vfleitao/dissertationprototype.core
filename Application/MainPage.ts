import Component from "~/Core/Framework/Components/Component";
import RootComponent from "~/Core/Framework/Components/RootComponent";
import ContainerComponent from "~/Core/Framework/Components/ContainerComponent";
import Orientation from "~/Core/Framework/Enums/Orientation";
import CreateNewItemFormComponent from "~/Core/Application/Components/CreateNewItemFormComponent";
import ToDoItemListComponent from "~/Core/Application/Components/ToDoItemListComponent";
import LabelComponent from "~/Core/Framework/Components/LabelComponent";
import ModuleResolver from "~/Core/Framework/IOCContainer/ModuleResolver";
import NotificationService from "~/Core/Framework/NotificationService/NotificationService";
import ToDoDataStore from "./DataStore/ToDoDataStore";
import Notifications from "./Consts/Notifications";
import ArrayUtils from "../Framework/Utils/ArrayUtils";


/**
 * Main Page used to generate a wrapper around the app
 *
 * @export
 * @class MainPage
 */
export default class MainPage {
    private notificationService: NotificationService;
    private toDoDataStore: ToDoDataStore;

    public PageRoot: Component;

    /**
     *Creates an instance of MainPage.
     * @memberof MainPage
     */
    constructor() {
        this.notificationService = ModuleResolver.Instance.GetModuleSync(
            "NotificationService"
        );
        this.toDoDataStore = ModuleResolver.Instance.GetModuleSync(
            "ToDoDataStore"
        );

        this.CreateItems();
    }

    private CreateItems() {
        this.PageRoot = new RootComponent();
        const container = new ContainerComponent({
            Appearance: "application",
            Orientation: Orientation.Vertical
        });
        const listItemContainer = new ContainerComponent({
            Appearance: "list-item-container",
            Orientation: Orientation.Vertical
        });
        
        listItemContainer.AddChild(
            new LabelComponent({
                Appearance: "list-title",
                InitialText: "To Do List"
            })
        );

        const noItemsLabel = new LabelComponent({
            Appearance: "list-no-items-label",
            InitialText: "No todo's in list"
        });

        const noItemsContainer = new ContainerComponent({
            Appearance: "list-no-items-container"
        });

        noItemsContainer.AddChild(noItemsLabel);

        listItemContainer.AddChild(noItemsContainer);

        listItemContainer.AddChild(new ToDoItemListComponent(null, {
            Appearance: "list",
            Orientation: Orientation.Vertical
        }));
        container.AddChild(listItemContainer);
        container.AddChild(new CreateNewItemFormComponent());
        this.PageRoot.AddChild(container);

        this.notificationService.Register({
            Context: {
                ContextKey: "MainPage"
            },
            NotificationHandler: () => {
                if (ArrayUtils.IsNullOrEmpty(this.toDoDataStore.Data)) {
                    noItemsContainer.AddChild(noItemsLabel);
                } else {
                    noItemsContainer.RemoveChild(noItemsLabel);
                }
            },
            NotificationName: Notifications.TODO_ITEMS_UPDATED
        });
    }
}
