import ListItemComponent from "~/Core/Framework/Components/ListItemComponent";
import ToDoModel from "~/Core/Application/Models/ToDoModel";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import ContainerComponent from "~/Core/Framework/Components/ContainerComponent";
import LabelComponent from "~/Core/Framework/Components/LabelComponent";
import Orientation from "~/Core/Framework/Enums/Orientation";
import ButtonComponent from "~/Core/Framework/Components/ButtonComponent";
import SwitcherComponent from "~/Core/Framework/Components/SwitcherComponent";
import Notifications from "~/Core/Application/Consts/Notifications";


/**
 * To Do Item displayed in a list
 *
 * @export
 * @class ToDoItemListItemComponent
 * @extends {ListItemComponent<ToDoModel>}
 */
export default class ToDoItemListItemComponent extends ListItemComponent<ToDoModel>
{
    private isOpen: boolean = false;
    private containerComponent: ContainerComponent;
    private mainSectionComponent: ContainerComponent;

    private titleComponent: LabelComponent;
    private deleteButton: ButtonComponent;
    private seeDetailButton: ButtonComponent;
    private descriptionComponent: LabelComponent;
    private completeComponent: SwitcherComponent;

    /**
     * Get the title from data
     *
     * @readonly
     * @private
     * @type {string}
     * @memberof ToDoItemListItemComponent
     */
    private get TitleText(): string {
        if (ObjectUtils.IsNullOrUndefined(this.Data)) {
            return "";
        }

        return this.Data.Title;
    }

    /**
     * Gets the description from data
     *
     * @readonly
     * @private
     * @type {string}
     * @memberof ToDoItemListItemComponent
     */
    private get DesciptionText(): string {
        if (ObjectUtils.IsNullOrUndefined(this.Data)) {
            return "";
        }

        return this.Data.Description;
    }

    /**
     * Gets the completed status from data
     *
     * @readonly
     * @private
     * @type {boolean}
     * @memberof ToDoItemListItemComponent
     */
    private get Complete(): boolean {
        if (ObjectUtils.IsNullOrUndefined(this.Data)) {
            return false;
        }

        return this.Data.Completed;
    }

    /**
     * Gets the To Do Id from data
     *
     * @readonly
     * @type {string}
     * @memberof ToDoItemListItemComponent
     */
    public get Id(): string {
        if (!this.Data) {
            return null;
        }

        return this.Data.Id;
    }

    constructor(data: ToDoModel) {
        super(data, {
            Appearance: "list-item"
        });
    }

    /**
     * On Data Set
     *
     * @protected
     * @returns
     * @memberof ToDoItemListItemComponent
     */
    protected OnDataSet() {
        if (ObjectUtils.IsNullOrUndefined(this.Data)) {
            return;
        }

        this.titleComponent.Text = this.Data.Title;
        this.descriptionComponent.Text = this.Data.Description;
        this.completeComponent.On = this.Data.Completed;
    }

    /**
     * On Component initialized
     *
     * @protected
     * @memberof ToDoItemListItemComponent
     */
    protected OnInitialize() {
        this.containerComponent = new ContainerComponent({
            Appearance: "list-item-wrapper",
            Orientation: Orientation.Vertical
        });
        this.SetupMainComponent();
        this.SetupDescriptionComponent();

        this.containerComponent.AddChild(this.mainSectionComponent);
        this.AddChild(this.containerComponent);
    }

    /**
     * Sets up the layout for the main component
     *
     * @private
     * @memberof ToDoItemListItemComponent
     */
    private SetupMainComponent() {
        this.mainSectionComponent = new ContainerComponent({
            Appearance: "list-item-main",
            Orientation: Orientation.Horizontal
        });

        this.deleteButton = new ButtonComponent({
            Appearance: "list-item-delete-button",
            InitialText: "X"
        });
        this.deleteButton.OnClick = this.OnDeleteButtonClick.bind(this);

        this.completeComponent = new SwitcherComponent({
            Appearance: "list-item-complete-switcher",
            On: this.Complete
        });
        this.titleComponent = new LabelComponent({
            Appearance: "list-item-title",
            InitialText: this.TitleText
        });

        this.seeDetailButton = new ButtonComponent({
            Appearance: "list-item-show-description-button",
            InitialText: "Description"
        });

        this.seeDetailButton.OnClick = this.OnToggleDescriptionClick.bind(this);

        this.mainSectionComponent.AddChild(this.completeComponent);
        this.mainSectionComponent.AddChild(this.titleComponent);
        this.mainSectionComponent.AddChild(this.seeDetailButton);
        this.mainSectionComponent.AddChild(this.deleteButton);
    }

    /**
     * On show Description
     *
     * @private
     * @memberof ToDoItemListItemComponent
     */
    private OnToggleDescriptionClick() {
        if (!this.isOpen) {
            this.containerComponent.AddChild(this.descriptionComponent);
        } else {
            this.containerComponent.RemoveChild(this.descriptionComponent);
        }

        this.isOpen = !this.isOpen;
    }

    /**
     * On Delete button clicked
     *
     * @private
     * @memberof ToDoItemListItemComponent
     */
    private OnDeleteButtonClick() {
        this.SendNotification(Notifications.REMOVE_TODO, this.Data, this.Id);
    }

    /**
     * Setting up the description
     *
     * @private
     * @memberof ToDoItemListItemComponent
     */
    private SetupDescriptionComponent() {
        this.descriptionComponent = new LabelComponent({
            Appearance: "list-item-description",
            InitialText: this.DesciptionText
        });
    }
}
