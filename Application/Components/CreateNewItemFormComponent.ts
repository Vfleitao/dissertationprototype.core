import FormComponent from "~/Core/Framework/Components/FormComponent";
import LabelComponent from "~/Core/Framework/Components/LabelComponent";
import ContainerComponent from "~/Core/Framework/Components/ContainerComponent";
import Orientation from "~/Core/Framework/Enums/Orientation";
import InputComponent from "~/Core/Framework/Components/InputComponent";
import MultilineInputComponent from "~/Core/Framework/Components/MultilineInputComponent";
import ButtonComponent from "~/Core/Framework/Components/ButtonComponent";
import SwitcherComponent from "~/Core/Framework/Components/SwitcherComponent";
import ToDoModel from "~/Core/Application/Models/ToDoModel";
import Notifications from "~/Core/Application/Consts/Notifications";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";

/**
 * Form used to create new ToDo items
 *
 * @export
 * @class CreateNewItemFormComponent
 * @extends {FormComponent}
 */
export default class CreateNewItemFormComponent extends FormComponent {
    private titleInput: InputComponent;
    private descriptionInput: InputComponent;
    private submitButton: ButtonComponent;
    private completedSwitcher: SwitcherComponent;

    /**
     * Creates an instance of CreateNewItemFormComponent.
     * @memberof CreateNewItemFormComponent
     */
    constructor() {
        super({ Appearance: "create-new-todo" });
        this.CreateItems();
    }

    /**
     * ToDo Model Serialization
     *
     * @protected
     * @returns {*}
     * @memberof CreateNewItemFormComponent
     */
    protected Serialize(): any {
        const todoModel = new ToDoModel();
        todoModel.Title = this.titleInput.Value;
        todoModel.Description = this.descriptionInput.Value;
        todoModel.Completed = this.completedSwitcher.On;

        return todoModel;
    }


    /**
     * Form component and layout setup
     *
     * @private
     * @memberof CreateNewItemFormComponent
     */
    private CreateItems() {
        this.AddChild(
            new LabelComponent({
                Appearance: "form-header",
                InitialText: "Add a new item"
            })
        );

        const titleContainer = new ContainerComponent({
            Appearance: "new-item-title-container",
            Orientation: Orientation.Horizontal
        });
        titleContainer.AddChild(
            new LabelComponent({
                Appearance: "new-item-title",
                InitialText: "Title"
            })
        );

        this.titleInput = new InputComponent({
            Appearance: "new-item-title-input"
        });
        titleContainer.AddChild(this.titleInput);

        const descriptionContainer = new ContainerComponent({
            Appearance: "new-item-description-container",
            Orientation: Orientation.Vertical
        });
        descriptionContainer.AddChild(
            new LabelComponent({
                Appearance: "new-item-description-label",
                InitialText: "Description"
            })
        );

        this.descriptionInput = new MultilineInputComponent({
            Appearance: "new-item-description-input"
        });
        descriptionContainer.AddChild(this.descriptionInput);

        const completedContainer = new ContainerComponent({
            Appearance: "new-item-complete-container",
            Orientation: Orientation.Horizontal
        });
        this.completedSwitcher = new SwitcherComponent({
            Appearance: "new-item-complete-switcher",
            On: false
        });
        completedContainer.AddChild(this.completedSwitcher);
        completedContainer.AddChild(
            new LabelComponent({
                Appearance: "new-item-complete-label",
                InitialText: "Complete"
            })
        );

        this.submitButton = new ButtonComponent({
            Appearance: "new-item-submit",
            InitialText: "Add new To Do"
        });
        this.submitButton.OnClick = this.OnSubmitPressed.bind(this);

        this.AddChild(titleContainer);
        this.AddChild(descriptionContainer);
        this.AddChild(completedContainer);
        this.AddChild(this.submitButton);
    }

    /**
     * Submit button handler
     * Will trigger a creation of the new
     * ToDo Item
     *
     * @private
     * @memberof CreateNewItemFormComponent
     */
    private OnSubmitPressed() {
        if (
            ObjectUtils.IsNullOrUndefined(this.titleInput.Value) ||
            ObjectUtils.IsNullOrUndefined(this.descriptionInput.Value) ||
            this.titleInput.Value == "" || 
            this.descriptionInput.Value == "") {
            return;
        }

        this.SendNotification(Notifications.ADD_NEW_TODO, this.Serialize(), this.Id);
        this.ResetForm();
    }

    private ResetForm() {
        this.completedSwitcher.On = false;
        this.titleInput.Value = "";
        this.descriptionInput.Value = "";
    }
}
