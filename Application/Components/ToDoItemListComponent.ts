import ListComponent from "~/Core/Framework/Components/ListComponent";
import ToDoItemListItemComponent from "~/Core/Application/Components/ToDoItemListItemComponent";
import ToDoModel from "~/Core/Application/Models/ToDoModel";
import ObjectUtils from "~/Core/Framework/Utils/ObjectUtils";
import Notifications from "~/Core/Application/Consts/Notifications";
import IContainerProperties from "~/Core/Framework/Components/Properties/IContainerProperties";

/**
 * To Do List
 *
 * @export
 * @class ToDoItemListComponent
 * @extends {ListComponent<ToDoModel, ToDoItemListItemComponent>}
 */
export default class ToDoItemListComponent extends ListComponent<ToDoModel, ToDoItemListItemComponent>
{

    /**
     * Creates an instance of ToDoItemListComponent.
     * @param {ToDoModel[]} [data]
     * @param {IContainerProperties} initialProperties
     * @memberof ToDoItemListComponent
     */
    constructor(data?: ToDoModel[], initialProperties?: IContainerProperties) {
        super(data, initialProperties);
    }

    /**
     * Registers a new Notification Handler
     *
     * @protected
     * @memberof ToDoItemListComponent
     */
    protected AddNotificationHandlers() {
        this.RegisterNotification(Notifications.TODO_ITEM_ADDED, this.OnToDoItemAdded.bind(this));
        this.RegisterNotification(Notifications.TODO_ITEM_REMOVED, this.OnToDoItemRemoved.bind(this));
    }


    /**
     * On a new To Do Added
     *
     * @private
     * @param {string} notificationName
     * @param {ToDoModel} body
     * @memberof ToDoItemListComponent
     */
    private OnToDoItemAdded(notificationName: string, body: ToDoModel) {
        if (!ObjectUtils.IsNullOrUndefined(body)) {
            this.AddNewItem(body);
        }
    }

    /**
     * On  To Do Removed
     *
     * @private
     * @param {string} notificationName
     * @param {ToDoModel} body
     * @memberof ToDoItemListComponent
     */
    private OnToDoItemRemoved(notificationName: string, body: ToDoModel) {
        if (!ObjectUtils.IsNullOrUndefined(body)) {
            this.RemoveItem(body);
        }
    }

    /**
     * Gets the Id of a To Do Item
     *
     * @protected
     * @param {ToDoModel} data
     * @returns {string}
     * @memberof ToDoItemListComponent
     */
    protected GetId(data: ToDoModel): string {
        if (ObjectUtils.IsNullOrUndefined(data)) {
            return null;
        }

        return data.Id;
    }

    /**
     * Creation of a To Do Component
     *
     * @protected
     * @param {ToDoModel} data
     * @returns {ToDoItemListItemComponent}
     * @memberof ToDoItemListComponent
     */
    protected CreateNewComponent(data: ToDoModel): ToDoItemListItemComponent {
        return new ToDoItemListItemComponent(data);
    }
}