/**
 *
 *
 * @export
 * @class ToDoModel
 */
export default class ToDoModel {
    public Id: string;
    public Title: string;
    public Description: string;
    public Completed: boolean;
}