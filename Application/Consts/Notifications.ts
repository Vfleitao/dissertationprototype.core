/**
 * Notification Names used in the system
 *
 * @export
 * @class Notifications
 */
export default class Notifications {
    public static ADD_NEW_TODO: string = "ADD_NEW_TODO";
    public static REMOVE_TODO: string = "REMOVE_TODO";
    public static TODO_ITEMS_UPDATED: string = "TODO_ITEMS_UPDATED";
    public static TODO_ITEM_ADDED: string = "TODO_ITEM_ADDED";
    public static TODO_ITEM_REMOVED: string = "TODO_ITEM_REMOVED";
}